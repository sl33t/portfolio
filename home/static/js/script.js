function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function removeBlog(item_id) {
    $("#blog_post" + item_id).hide();
    $.post("/blogChange/remove/" + item_id + "/");
}

function removePortfolio(item_id) {
    $("#portfolio_item" + item_id).hide();
    $.post("/portfolioChange/remove/" + item_id + "/");
}

function dropdown() {
    $(".dropdown").toggle();
}

window.onresize = function (event) {
    if (screen.width > 1280) {
        var mobilelinks = document.getElementsByClassName("mobilelinks");
        for (var i = 0; i < mobilelinks.length; i++) {
            mobilelinks[i].removeAttribute('style');
        }
        document.getElementById("nav").removeAttribute('style');
    }
};